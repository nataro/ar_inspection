from typing import List, AnyStr, Dict, Optional, Sequence, Tuple
import pathlib

import cv2
import numpy as np


class DetectConnectedComponents(object):
    def __init__(self,
                 ksize: Tuple[int] = (3, 3),
                 binary_inv: bool = False,
                 ):
        self._morph_ksize = ksize
        self._thresh_type = cv2.THRESH_BINARY
        if binary_inv:
            self._thresh_type = cv2.THRESH_BINARY_INV

    @staticmethod
    def draw_component_rectangle(
            img: np.ndarray,
            status: Tuple[int, int, int, int, int, List[float]],
            thickness: int = 1,
            color: Tuple[int, int, int] = (0, 255, 0),
    ):
        """
        maximum_component_stats()で得たstatusを渡してサークルを描画する
        Parameters
        ----------
        img
        status
        thickness
        color

        """
        x, y, w, h, area, centroid = status
        # print(x, y, w, h, area, centroid)
        cv2.rectangle(img, (x, y), (x + w, y + h), color, thickness=thickness)

    @staticmethod
    def maximum_component_stats(components_stats: Tuple) -> Tuple[int, int, int, int, int, List[float]]:
        """
        cv2.connectedComponentsWithStats()から出力される(n_labels, labels, stats, centroids)を受け取って、
        検出したcomponetのうち最も大きいものの情報を返す

        Parameters
        ----------
        components_stats

        Returns
        -------
           (左上x座標、左上y座標、幅、高さ、連結成分の総面積、重心）
        """
        n_labels, labels, stats, centroids = components_stats

        # cv2.CC_STAT_AREAは連結成分の総面積
        area = stats[:, cv2.CC_STAT_WIDTH] * stats[:, cv2.CC_STAT_HEIGHT]

        # 2番目に面積が大きいラベルを小さい順にソート後に取得 (1番目は背景なので除く)
        # ラベル数が2以下の場合は、外枠を検出している思われる
        top_idx = 0 if n_labels <= 2 else area.argsort()[-2]

        maxmum_stats = (
            stats[top_idx, cv2.CC_STAT_LEFT],
            stats[top_idx, cv2.CC_STAT_TOP],
            stats[top_idx, cv2.CC_STAT_WIDTH],
            stats[top_idx, cv2.CC_STAT_HEIGHT],
            stats[top_idx, cv2.CC_STAT_AREA],
            centroids[top_idx],
        )

        return maxmum_stats

    def morpholoty(self, img: np.ndarray, opening: int = 0, closing: int = 0) -> np.ndarray:
        kernel = np.ones(self._morph_ksize, np.uint8)
        if (times := opening) != 0:
            img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel, iterations=times)
        if (times := closing) != 0:
            img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=times)
        return img

    def processing(self, img: np.ndarray, thresh: int = 150, opening: int = 0, closing: int = 0) -> Tuple[
        Tuple, np.ndarray]:

        # グレースケール化
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # cv2.THRESH_BINARY にて２値化する
        ret, gray_img = cv2.threshold(gray_img, thresh, 255, self._thresh_type)

        # モルフォロジー処理
        gray_img = self.morpholoty(gray_img, opening, closing)

        # 結果書き出し用にグレースケールをカラー変換
        color_img = cv2.cvtColor(gray_img, cv2.COLOR_GRAY2BGR)

        # ラベリング結果
        stats_data = cv2.connectedComponentsWithStats(gray_img)

        # 検出した最大のコンポーネントの情報のみ取り出す
        max_stats_data = self.maximum_component_stats(stats_data)

        return max_stats_data, color_img
