from typing import List, AnyStr, Dict, Optional, Sequence, Tuple
import csv
import itertools
from logging import getLogger, StreamHandler, FileHandler, DEBUG

from util.datasets import InspectionResultDataset

import numpy as np
import cv2
from cv2 import aruco

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
# # handler = FileHandler('dousa_log.txt')
# handler.setLevel(DEBUG) # 出力するログのレベルを設定(デフォルトはNOTSET)
logger.addHandler(handler)


def trans_pos(trans_mat: np.array, target_pos: np.array) -> np.array:
    """
    透視投影の座標変換を行う。具体的な数値の遷移は以下参照。

    trans_mat:
    [ 3.15652152e+00 -7.16209823e-01 -1.66453805e+03]
    [-3.08903449e-01  3.93942752e+00 -1.40031748e+03]
    [-4.23401823e-04  5.10348019e-04  1.00000000e+00]

    target_pos:
    [516.5 409.5]

    np.append(target_pos,1):
    [516.5 409.5   1. ]

    trans_mat@target_pos:
    [-327.48260978   53.32945487    0.99030047]

    target_pos_trans[2]:
    0.9903004725337483

    target_pos_trans/target_pos_trans[2]:
    [-330.69014795   53.85179181    1.        ]

    """
    target_pos = np.append(target_pos, 1)
    target_pos_trans = trans_mat @ target_pos
    target_pos_trans = target_pos_trans / target_pos_trans[2]
    return target_pos_trans[:2]


def camera2screen(camera_mat: np.array, world_coord: np.array) -> np.array:
    '''
    カメラ座標系の3次元座標を画像座標系の2次元座標に変換
    キャリブレーションで求めた3x3のカメラマトリックスを使用する

    camera_mat=np.asarray(
        [[1.33862088e+03, 0.00000000e+00, 9.85690260e+02],
        [0.00000000e+00, 1.39737705e+03, 4.92773839e+02],
        [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

    world_coord=np.asarray([0.1, 0.2, 0.5 ])

    image_pos = camera_mat@world_coord:
    [6.26707218e+02 5.25862329e+02 5.00000000e-01]

    image_pos/image_pos[2]:
    [1.25341444e+03 1.05172466e+03 1.00000000e+00]

    '''
    image_pos = camera_mat @ world_coord
    image_pos = image_pos / image_pos[2]
    return image_pos[:2]


def screen2camera(camera_mat: np.array, screen_coord: np.array, camera_z_code: float = 1.0) -> np.array:
    """
    画像座標系（2次元）をカメラ座標系（3次元）に変換。
    変換時にカメラ座標系のZ座標のみ指定する必要がある。
    キャリブレーションで求めた3x3のカメラマトリックスを使用する。

    camera_mat=np.asarray(
        [[1.33862088e+03, 0.00000000e+00, 9.85690260e+02],
        [0.00000000e+00, 1.39737705e+03, 4.92773839e+02],
        [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

    screen_coord=np.asarray([1.25341444e+03, 1.05172466e+03])

    screen_pos=np.append(screen_coord,1.0):
    [1.25341444e+03, 1.05172466e+03, 1.00000000e+00]

    screen_pos *= camera_z_code:(z=0.5とする)
    [6.26707218e+02, 5.25862329e+02, 5.00000000e-01]

    camera_coord=np.linalg.inv(camera_mat)@screen_pos:
    [0.1, 0.2, 0.5 ]

    """
    screen_pos = np.append(screen_coord, 1.0)
    screen_pos *= camera_z_code
    camera_coord = np.linalg.inv(camera_mat) @ screen_pos
    return camera_coord


def load_parameter(file_name: str):
    with open(file_name) as f:
        reader = csv.reader(f)
        return [[float(s) for s in row] for row in reader]


def load_and_check_parameter(
        camera_csv_file: str,
        dist_csv_file: str,
) -> Optional[Tuple[np.ndarray, np.ndarray]]:
    try:
        cam_mat = np.asarray(load_parameter(camera_csv_file))
        dist_mat = np.asarray(load_parameter(dist_csv_file))
        return cam_mat, dist_mat
    except FileNotFoundError as e:
        logger.error(e)


class DetectArMarkers(object):
    def __init__(self,
                 camera_matrix: np.ndarray,
                 distortion_coefs: np.ndarray,
                 marker_length: float = 0.03,  # マーカの大きさをmで指定
                 ar_dict: cv2.aruco_Dictionary = aruco.DICT_4X4_250
                 ):
        self._cam_mat = camera_matrix
        self._dist_coefs = distortion_coefs
        self._dictionary = aruco.getPredefinedDictionary(ar_dict)
        self._marker_length = marker_length

        # detectMarkers()に渡すパラメータ （必須ではない？）
        self._ar_parameters = aruco.DetectorParameters_create()
        self._ar_parameters.cornerRefinementMethod = aruco.CORNER_REFINE_CONTOUR

    @classmethod
    def load_and_check_parameter(
            cls,
            camera_csv_file: str,
            dist_csv_file: str,
    ) -> Optional[Tuple[np.ndarray, np.ndarray]]:
        return load_and_check_parameter(camera_csv_file, dist_csv_file)

    def detect_single_markers(self, gray_img: np.ndarray, ) -> List:
        """
        aruco.estimatePoseSingleMarkers()では、マーカーの中心をマーカー座標系の原点として結果を出力していると思われる

        Parameters
        ----------
        gray_img

        Returns
        -------

        """
        # とりあえずマーカー類を全部個別に検出
        # rejectedPoints : 正しく検出できなかったマーカーの情報？デバッグ目的
        corners, ids, rejectedPoints = aruco.detectMarkers(gray_img, self._dictionary, parameters=self._ar_parameters)

        detect_markers = []

        if len(corners) <= 0:
            return detect_markers

        for corner, id in zip(corners, ids):
            rvec, tvec, _ = aruco.estimatePoseSingleMarkers(
                corner, self._marker_length,
                self._cam_mat, self._dist_coefs,
                None, None)

            # 不要なaxisを除去
            tvec = np.squeeze(tvec)
            rvec = np.squeeze(rvec)
            # # 回転ベクトルからrodoriguesへ変換
            # rvec_matrix = cv2.Rodrigues(rvec)
            # rvec_matrix = rvec_matrix[0]  # rodoriguesから抜き出し
            # # 並進ベクトルの転置
            # transpose_tvec = tvec[np.newaxis, :].T
            # # 合成
            # proj_matrix = np.hstack((rvec_matrix, transpose_tvec))
            # # オイラー角への変換
            # euler_angle = cv2.decomposeProjectionMatrix(proj_matrix)[6]  # [deg]

            detect_markers.append((corner, id, tvec, rvec))

        return detect_markers

    def processing(self, img: np.ndarray) -> Tuple[Tuple, np.ndarray]:

        # グレースケール化
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # シングルマーカー検出結果
        marker_stats = self.detect_single_markers(gray_img)

        return marker_stats, gray_img

    def camera2screen(self, world_coord: np.array) -> np.array:
        return camera2screen(self._cam_mat, world_coord)

    def screen2camera(self, screen_coord: np.array, camera_z_code: float = 1.0) -> np.array:
        return screen2camera(self._cam_mat, screen_coord, camera_z_code)

    def analyzed_info_calculate(
            self,
            com_state: Tuple[int, int, int, int, int, List[float]],
            ar_state: Tuple[List[List[float]], List[int], List[float], List[float]],
            marker_offset: Tuple[int, int] = (0, 0),
    ) -> InspectionResultDataset:
        """
        com_state = (
            479,  # left
            490,  # top
            814,  # width
            386,  # height
            198343,  # area
            array([856.42120468, 679.50783743]))  # centroids

        ar_state = (
            array([[[1111.9412 ,  640.58966],
                [1153.6708 ,  634.8143 ],
                [1159.7457 ,  676.6903 ],
                [1118.1902 ,  681.85974]]], dtype=float32), # corners
            array([5], dtype=int32),  # id
            array([0.03898361, 0.02377449, 0.31059862]),  # tvec
            array([-2.92360508,  0.22973807,  0.18517742])  # rvec
        )


        Returns
        -------

        """
        c_left, c_top, c_width, c_height, c_area, c_centroids = com_state
        a_corners, a_id, a_tvec, a_rvec = ar_state

        # マーカの基準点をマーカ座標系上で指定（オフセットが(0,0)の場合はマーカの中心）
        ar_mrk_coord = np.asarray(marker_offset + (0,))  # マーカー位置（マーカ座標系）

        tvec = np.asarray(a_tvec)  # tvec
        rvec = np.asarray(a_rvec)  # rvec

        # 検出したマーカ位置
        ar_cam_coord = rvec @ ar_mrk_coord + tvec  # マーカー位置（カメラ座標系）
        ar_scr_coord = self.camera2screen(ar_cam_coord)  # マーカー位置（画像座標系）

        # 原点は検出した枠の左上
        org_scr_coord = np.asarray([c_left, c_top])  # 原点（画像座標系）
        org_cam_coord = self.screen2camera(org_scr_coord, ar_cam_coord[2])  # 原点（カメラ座標系）

        # 検出した枠の右下
        br_scr_coord = np.asarray([c_left + c_width, c_top + c_height])  # 枠の右下（画像座標系）
        br_cam_coord = self.screen2camera(br_scr_coord, ar_cam_coord[2])  # 原点（カメラ座標系）

        # 枠の中心（カメラ座標系）
        cc_cam_coord = org_cam_coord + (br_cam_coord - org_cam_coord) / 2

        dataset = InspectionResultDataset(
            com_left_pix=c_left,
            com_top_pix=c_top,
            com_width_pix=c_width,
            com_height_pix=c_height,
            com_area_pix=c_area,
            com_centroid_x_pix=round(c_centroids[0], 1),
            com_centroid_y_pix=round(c_centroids[1], 1),
            ar_x_pix=int(ar_scr_coord[0]),
            ar_y_pix=int(ar_scr_coord[1]),
            ar_id=int(a_id),
            ar_from_left=round(ar_cam_coord[0] - org_cam_coord[0], 4),
            ar_from_top=round(ar_cam_coord[1] - org_cam_coord[1], 4),
            ar_from_ccx=round(ar_cam_coord[0] - cc_cam_coord[0], 4),
            ar_from_ccy=round(ar_cam_coord[1] - cc_cam_coord[1], 4),
            com_width=round(br_cam_coord[0] - org_cam_coord[0], 4),
            com_height=round(br_cam_coord[1] - org_cam_coord[1], 4),
        )

        return dataset


class DetectChessBoard(object):
    def __init__(self,
                 col_intersections: int = 7,  # 列方向の交点数
                 row_intersections: int = 6,  # 行方向の交点数
                 square_length: float =0.022,  # チェスボードの１桝の正方形寸法をmで指定
                 ):
        self._col = col_intersections
        self._row = row_intersections
        self._length = square_length

        # prepare object points
        self._pattern_points = np.zeros((self._col * self._row, 3), np.float32)
        self._pattern_points[:, :2] = np.mgrid[0:self._col, 0:self._row].T.reshape(-1, 2)
        self._pattern_points *= self._length

        # Arrays to store object points and image points from all the images.
        self._objpoints = []  # 3d point in real world space
        self._imgpoints = []  # 2d points in image plane.

        # termination criteria
        self._criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    def processing(self, color_img: np.ndarray) -> Tuple[bool, np.ndarray]:
        # グレースケール化
        gray_img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

        # print(gray_img.shape, gray_img.shape[::-1])

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray_img, (self._col, self._row), None)

        if ret:
            corners_fine = cv2.cornerSubPix(gray_img, corners, (11, 11), (-1, -1), self._criteria)
            self._objpoints.append(self._pattern_points)
            self._imgpoints.append(corners_fine)
            color_img = cv2.drawChessboardCorners(color_img, (self._col, self._row), corners_fine, ret)

        return ret, color_img

    def get_captured_num(self) -> int:
        if (num := len(self._objpoints)) == len(self._imgpoints):
            return num
        return None

    def calibrate(self, img_size: Tuple[int, int]) -> Tuple:
        """
        cv2.calibrateCamera()に渡すimg_size は　(width, height)で渡さないといけないと思う
        Parameters
        ----------
        img_size

        Returns
        -------

        """
        # Calc urate the camera matrix
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(self._objpoints, self._imgpoints, img_size, None, None)

        return ret, mtx, dist,

