from logging import getLogger, StreamHandler, FileHandler, DEBUG, INFO
from typing import Optional, Tuple, Sequence, List

from pathlib import Path
from datetime import datetime
import numpy as np

import cv2

from dev.cam_capture import CaptureDevice
from processor.detect_ar import DetectArMarkers
from processor.detect_component import DetectConnectedComponents

from util.datasets import get_values_csv

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
# # handler = FileHandler('dousa_log.txt')
# handler.setLevel(DEBUG) # 出力するログのレベルを設定(デフォルトはNOTSET)
logger.addHandler(handler)


class ArMarkerAnalyze(object):
    def __init__(self,
                 detect_marker: DetectArMarkers,
                 detect_componet: DetectConnectedComponents,
                 cam_num: int = 0,
                 main_window_type: int = cv2.WINDOW_AUTOSIZE,
                 cap_size: Tuple[int, int] = (1920, 1080),
                 disp_size: Optional[Tuple[int, int]] = None,
                 cap_mode: str = 'dshow',
                 cap_time_msec: int = 1000,
                 img_dir: str = './output_img/',
                 ):
        """
        Parameters
        ----------
        detect_marker : DetectArMarkers
        detect_componet : DetectConnectedComponents
        cam_num : opencvで制御するカメラデバイス指定
        main_window_type :
            cv2.WINDOW_AUTOSIZE：デフォルト。ウィンドウ固定表示
            cv2.WINDOW_NORMAL：ウィンドウのサイズを変更可能にする
        cap_size : カメラで取得する原画像のサイズ
        disp_size : GUI上で表示する際の画像サイズ
        cap_mode :
            'dshow': cv2.CAP_DSHOW,
            'msmf': cv2.CAP_MSMF,
            'v4l2': cv2.CAP_V4L2,
        cap_time_msec : 画像を解析した際に待機する画面表示時間
        img_dir : 解析結果画像を出力するディレクトリ
        """
        self._darm = detect_marker
        self._dcom = detect_componet
        self._main_window_name = 'window'
        self._main_window_type = main_window_type
        self._cam_num = cam_num
        self._cap_size = cap_size
        self._cap_mode = cap_mode
        self._disp_size = cap_size if self._cap_size is None else disp_size
        self._cap_time_msec = cap_time_msec

        self._track_window_name = 'trackbars'
        self._thresh: int = 150
        self._opening: int = 0
        self._closing: int = 0
        self._roi_rate: float = 1.0

        # 解析結果出力画像関係
        self._font_size = 2
        # 画像の出力先
        self._img_output_dir_p = Path(img_dir)
        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に
        self._img_output_dir_p.mkdir(exist_ok=True, parents=True)

    @staticmethod
    def trackbar_callback(position):
        pass

    @staticmethod
    def clip_roi_img(img: np.ndarray, roi_rate: float = 1.0) -> np.ndarray:
        if roi_rate == 1.0:
            return img
        rate = 0.1 if roi_rate < 0.1 else roi_rate
        h, w, c = img.shape

        h_roi = int(h * rate)
        w_roi = int(w * rate)

        h_begin = (h - h_roi) // 2
        w_begin = (w - w_roi) // 2

        return img[h_begin:h_begin + h_roi, w_begin:w_begin + w_roi, :]

    @staticmethod
    def put_line_feed_texts(
            img: np.ndarray,
            texts: Sequence[str],
            pos: Tuple[int, int] = (0, 0),
            font_scale: int = 1,
            font_color: Tuple[int, int, int] = (0, 255, 0),
    ):
        font_height = 18
        font_size = font_height * font_scale
        x, y = pos[0], pos[1] + font_size
        for i, t in enumerate(texts):
            cv2.putText(img, t, (x, y + font_size * i), cv2.FONT_HERSHEY_PLAIN, font_scale, font_color)

    def run(self):
        try:
            # ウィンドウの表示形式の設定
            cv2.namedWindow(self._main_window_name, self._main_window_type)
            cv2.namedWindow(self._track_window_name, cv2.WINDOW_NORMAL)

            # トラックバーを作成する
            cv2.createTrackbar("threshold", self._track_window_name, self._thresh, 255, self.trackbar_callback)
            cv2.createTrackbar("opening", self._track_window_name, self._opening, 10, self.trackbar_callback)
            cv2.createTrackbar("closing", self._track_window_name, self._closing, 10, self.trackbar_callback)
            cv2.createTrackbar("roi", self._track_window_name, int(self._roi_rate * 10), 10, self.trackbar_callback)

            self.show_window()
        except TypeError as e:
            logger.error(e)
        except OSError as e:
            logger.error(e)

    def show_window(self):

        with CaptureDevice(
                cam_num=self._cam_num,
                width=self._cap_size[0],
                height=self._cap_size[1],
                cap_mode=self._cap_mode, ) as cap:

            while True:
                # トラックバーの位置をthreshholdに代入
                self._thresh = cv2.getTrackbarPos("threshold", self._track_window_name)
                self._opening = cv2.getTrackbarPos("opening", self._track_window_name)
                self._closing = cv2.getTrackbarPos("closing", self._track_window_name)
                self._roi_rate = float(cv2.getTrackbarPos("roi", self._track_window_name) / 10)

                img = cap()

                if img is None:
                    break

                # 注目領域（中央付近）を切り取る
                img = self.clip_roi_img(img, self._roi_rate)

                com_stats, bin_color_img = self._dcom.processing(
                    img,
                    self._thresh,
                    self._opening,
                    self._closing
                )

                # 矩形を描画
                self._dcom.draw_component_rectangle(bin_color_img, com_stats, thickness=2, color=(0, 0, 255))

                # 表示用
                disp = cv2.resize(bin_color_img, self._disp_size)

                # キー入力の説明文字を追加
                texts = [
                    'c: Capture the image',
                    'q: Finish capturing',
                ]
                self.put_line_feed_texts(disp, texts, pos=(20, 0), font_scale=2)

                cv2.imshow(self._main_window_name, disp)

                k = cv2.waitKey(1) & 0xFF

                if k == ord('q'):
                    break

                if k == ord('c'):
                    # データ識別用の名前
                    dt = datetime.now()
                    info_name = dt.strftime("%Y%m%d-%H%M%S-%f")

                    ar_stats, gray_img = self._darm.processing(img)

                    # 画像に記入する文字列のリスト
                    texts = []
                    for i, s in enumerate(ar_stats):
                        dataset = self._darm.analyzed_info_calculate(com_stats, s)
                        dataset.name = info_name

                        # logging
                        # データセットのvaluesをcsv文字列でロギング
                        logger.info(get_values_csv(dataset))

                        texts.append(
                            f"{dataset.ar_id}, "
                            f"l:{dataset.ar_from_left}, t:{dataset.ar_from_top}, "
                            f"cx:{dataset.ar_from_ccx}, cy:{dataset.ar_from_ccy}"
                        )

                        corners = s[0]
                        ar_id = dataset.ar_id
                        cv2.aruco.drawDetectedMarkers(img, np.asarray([corners]), np.asarray([ar_id]), (0, 255, 0))
                        cv2.circle(img, (dataset.ar_x_pix, dataset.ar_y_pix), 4, (0, 0, 255), -1)

                    # 文字列を描画
                    self.put_line_feed_texts(img, texts, pos=(20, 0), font_scale=self._font_size)

                    # 矩形を描画
                    self._dcom.draw_component_rectangle(img, com_stats, thickness=2, color=(0, 255, 0))

                    # 画像を保存
                    cv2.imwrite(str(self._img_output_dir_p) + f"/{info_name}.jpg", img)

                    # 表示用
                    disp = cv2.resize(img, self._disp_size)
                    cv2.imshow(self._main_window_name, disp)
                    cv2.waitKey(self._cap_time_msec)


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    # マーカーサイズ
    marker_length = 0.012  # [m]
    cam_matrix_filename = "mtx.csv"
    distortion_filename = "dist.csv"

    cam_mat, dist_mat = DetectArMarkers.load_and_check_parameter(cam_matrix_filename, distortion_filename)

    dar = DetectArMarkers(cam_mat, dist_mat, marker_length)
    dcc = DetectConnectedComponents()
    ara = ArMarkerAnalyze(
        detect_marker=dar,
        detect_componet=dcc,
        cap_size=(1920, 1080),
        # disp_size=(640, 360),
        disp_size=(1280, 720),
    )
    ara.run()
