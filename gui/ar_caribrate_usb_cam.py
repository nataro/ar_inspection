from logging import getLogger, StreamHandler, FileHandler, DEBUG, INFO
from typing import Optional, Tuple, Sequence, List

import numpy as np

import cv2

from dev.cam_capture import CaptureDevice
from processor.detect_ar import DetectChessBoard

logger = getLogger(__name__)
logger.setLevel(DEBUG)  # Handlerに渡すログのレベル
handler = StreamHandler()
# handler.setLevel(DEBUG) # 出力するログのレベルを設定(デフォルトはNOTSET)
logger.addHandler(handler)


class ChessBoardCalibrate(object):
    def __init__(self,
                 detect_chess: DetectChessBoard,
                 output_cam_file: str = 'mtx.csv',
                 output_dist_file: str = 'dist.csv',
                 cam_num: int = 0,
                 main_window_type: int = cv2.WINDOW_AUTOSIZE,
                 cap_size: Tuple[int, int] = (1920, 1080),
                 disp_size: Optional[Tuple[int, int]] = None,
                 cap_mode: str = 'dshow',
                 cap_time_msec: int = 500,
                 ):
        """

        Parameters
        ----------
        detect_chess
        cam_num : opencvで制御するカメラデバイス指定
        main_window_type :
            cv2.WINDOW_AUTOSIZE：デフォルト。ウィンドウ固定表示
            cv2.WINDOW_NORMAL：ウィンドウのサイズを変更可能にする
        cap_size : カメラで取得する原画像のサイズ
        disp_size : GUI上で表示する際の画像サイズ
        cap_mode :
            'dshow': cv2.CAP_DSHOW,
            'msmf': cv2.CAP_MSMF,
            'v4l2': cv2.CAP_V4L2,
        cap_time_msec : 画像を解析した際に待機する画面表示時間
        """
        self._dche = detect_chess
        self._cam_file = output_cam_file
        self._dist_file = output_dist_file
        self._main_window_name = 'window'
        self._main_window_type = main_window_type
        self._cam_num = cam_num
        self._cap_size = cap_size
        self._cap_mode = cap_mode
        self._disp_size = cap_size if self._cap_size is None else disp_size
        self._cap_time_msec = cap_time_msec

        # 解析結果出力画像関係
        self._font_size = 2

    @staticmethod
    def put_line_feed_texts(
            img: np.ndarray,
            texts: Sequence[str],
            pos: Tuple[int, int] = (0, 0),
            font_scale: int = 1,
            font_color: Tuple[int, int, int] = (0, 255, 0),
    ):
        font_height = 18
        font_size = font_height * font_scale
        x, y = pos[0], pos[1] + font_size
        for i, t in enumerate(texts):
            cv2.putText(img, t, (x, y + font_size * i), cv2.FONT_HERSHEY_PLAIN, font_scale, font_color)

    def calibrate(self, img_size: Tuple[int, int]) -> bool:
        num = self._dche.get_captured_num()
        if (num is None) or (num < 10):
            logger.error(f"Calibrate error. At least 10 images must be taken. Now the num is {num}. ")
            return False
        ret, mtx, dist = self._dche.calibrate(img_size)
        if ret:
            np.savetxt(self._cam_file, mtx, delimiter=",")
            np.savetxt(self._dist_file, dist, delimiter=",")
            logger.info(f"Successful calibration.")
            return True
        else:
            logger.error('Calibrate error.')
            return False

    def run(self):
        try:
            # ウィンドウの表示形式の設定
            cv2.namedWindow(self._main_window_name, self._main_window_type)
            self.show_window()

        except TypeError as e:
            logger.error(e)
        except OSError as e:
            logger.error(e)

    def show_window(self):

        with CaptureDevice(
                cam_num=self._cam_num,
                width=self._cap_size[0],
                height=self._cap_size[1],
                cap_mode=self._cap_mode, ) as cap:

            while True:
                img = cap()

                if img is None:
                    break

                # 表示用
                disp = cv2.resize(img, self._disp_size)

                # キー入力の説明文字を追加
                texts = [
                    f'Number of capture : {self._dche.get_captured_num()}',
                    'c: Capture the image',
                    'q: Finish capturing and calculate the camera matrix and distortion',
                ]
                self.put_line_feed_texts(disp, texts, pos=(20, 0), font_scale=2)

                cv2.imshow(self._main_window_name, disp)

                k = cv2.waitKey(1) & 0xFF

                if k == ord('q'):
                    break

                if k == ord('c'):

                    ret, img = self._dche.processing(img)

                    if not ret:
                        texts = ['Can not find chessboard!', ]
                        self.put_line_feed_texts(img, texts, pos=(20, 0), font_scale=3, font_color=(0, 0, 255))

                    # 表示用
                    disp = cv2.resize(img, self._disp_size)
                    cv2.imshow(self._main_window_name, disp)
                    cv2.waitKey(self._cap_time_msec)

            # 終了前にキャリブレーションしてファイルを保存する
            # print(img.shape, img.shape[::-1])
            ret = self.calibrate((img.shape[1], img.shape[0]))


if __name__ == "__main__":
    """
    深い階層にある*.pyを単体で実行する際に、下記のようにモジュールをImportできずに
    エラーとなることがある。

    $ python util\measure_manager.py
    ImportError: attempted relative import with no known parent package

    このような場合、「.py」をつけずにモジュールとして実行すると良い
    $ python -m util.measure_mamager

    参考
    https://note.nkmk.me/python-relative-import/
    """

    col_intersections = 7  # 列方向の交点数
    row_intersections = 6  # 行方向の交点数
    square_length = 0.022  # チェスボードの１桝の正方形寸法をmで指定

    cam_matrix_filename = "mtx.csv"
    distortion_filename = "dist.csv"

    dcb = DetectChessBoard(col_intersections, row_intersections, square_length)

    cbc = ChessBoardCalibrate(
        detect_chess=dcb,
        output_cam_file=cam_matrix_filename,
        output_dist_file=distortion_filename,
        cap_size=(1920, 1080),
        disp_size=(1280, 720),
    )
    cbc.run()
