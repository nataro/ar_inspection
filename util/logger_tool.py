import logging

from logging import getLogger, Formatter, StreamHandler, FileHandler, DEBUG, ERROR

from typing import Optional, Sequence

'''
これを参考にした
https://own-search-and-study.xyz/2019/10/20/python-logging-clear/


RotatingFileHandlerを使ったこっちの方がいいかも
https://gist.github.com/arduino12/144c346c9f3ecc8175be45a2f6bda599

'''




class FormatterWithHeader(logging.Formatter):
    """
    Header を設定できるformatter

    https://stackoverflow.com/questions/33468174/write-header-to-a-python-log-file-but-only-if-a-record-gets-written

    """

    def __init__(
            self,
            header: Optional[str] = None,
            fmt: Optional[str] = None,
            datefmt: Optional[str] = None,
            style: str = '%'
    ):
        super().__init__(fmt, datefmt, style)
        self._header = header  # This is hard coded but you could make dynamic
        # Override the normal format method
        if self._header is not None:
            self.format = self.first_line_format

    def first_line_format(self, record):
        # First time in, switch back to the normal format function
        self.format = super().format
        return self._header + "\n" + self.format(record)


def get_logger_with_presets(
        logger_name: str,
        log_file: Optional[str] = None,
        header: Optional[str] = None,
        format: Optional[str] = None,
        date_format: Optional[str] = None,
        level: int = DEBUG,
) -> logging.Logger:
    """
    log_fileが設定されていた場合はFileHandlerに出力

    :param logger_name:
    :param log_file:
    :param format:
    :param date_format:
    :param level:
    :return: 設定済みのloggerを返す
    """
    logger = getLogger(logger_name)
    logger.setLevel(level)

    # 一旦設定済みのハンドラを削除する
    kill_all_handlers(logger)

    # formatter = Formatter(fmt=format, datefmt=date_format)
    formatter = FormatterWithHeader(header=header, fmt=format, datefmt=date_format)
    # formatter = None if format is None else Formatter(format)
    hd = StreamHandler() if log_file is None else FileHandler(log_file)
    hd.setFormatter(formatter)
    logger.addHandler(hd)
    return logger


def kill_logger(logger: logging.Logger):
    """
    指定したloggerを削除する
    :param logger:
    :return:
    """
    name = logger.name
    del logging.Logger.manager.loggerDict[name]


def kill_all_handlers(logger: logging.Logger):
    """
    loggerに設定されているhandlerを全て削除する
    :param logger:
    :return:
    """
    for h in logger.handlers:
        logger.removeHandler(h)


def kill_handler(logger: logging.Logger, handlers: Sequence[logging.Handler]):
    """
    loggerから特定のハンドルを削除する
    :param logger:
    :param handlers:
    :return:
    """
    for handler in handlers:
        logger.removeHandler(handler)
