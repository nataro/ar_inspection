from typing import List, AnyStr, Dict, Optional, Tuple
import dataclasses
import math


def get_values_list(data: dataclasses) -> Optional[List]:
    if data is None:
        return
    data_dic = dataclasses.asdict(data)
    return list(data_dic.values())


def get_values_csv(data: dataclasses) -> Optional[AnyStr]:
    v_list = get_values_list(data)
    if v_list is None:
        return
    return ','.join(map(str, v_list))


def get_keys_list(data: dataclasses) -> Optional[List]:
    if data is None:
        return
    data_dic = dataclasses.asdict(data)
    return list(data_dic.keys())


def get_keys_csv(data: dataclasses) -> Optional[AnyStr]:
    k_list = get_keys_list(data)
    if k_list is None:
        return
    return ','.join(map(str, k_list))


@dataclasses.dataclass
class InspectionResultDataset(object):
    """
    検査結果を格納するdataclasses
    """
    # データ識別用の名前（保存した画像ファイル名と合わせている）
    name: Optional[AnyStr] = None
    # 物体検出に係わる結果
    com_left_pix: int = 0
    com_top_pix: int = 0
    com_width_pix: int = 0  # 検出した物体領域の横幅（画素値）
    com_height_pix: int = 0  # 検出した物体領域の縦幅（画素値）
    com_area_pix: int = 0  # 検出した物体の画素値面積
    com_centroid_x_pix: float = 0.0  # 検出した物体の重心x
    com_centroid_y_pix: float = 0.0  # 検出した物体の重心y

    # ARマーカ検出に係わる結果
    ar_x_pix: Optional[int] = None  # 検出したARマーカの位置x（画素）
    ar_y_pix: Optional[int] = None  # 検出したARマーカの位置y（画素）
    ar_from_ccx: Optional[float] = None  # 検出したARマーカの位置（物体領域の中心x基準）←重心ではない
    ar_from_ccy: Optional[float] = None  # 検出したARマーカの位置（物体領域の中心y基準）←重心ではない

    ar_id: Optional[int] = None  # 検出したARマーカのid
    ar_from_left: Optional[float] = None  # 検出したARマーカの位置（物体領域の左辺基準）
    ar_from_top: Optional[float] = None  # 検出したARマーカの位置（物体領域の上辺基準）
    # 計算して取得する予定の情報
    ar_r_from_cc: Optional[float] = None
    com_width: Optional[float] = None  # 検出した物体領域の横幅（m）
    com_height: Optional[float] = None  # 検出した物体領域の縦幅（m）

    # com_aspect_wph: Optional[float] = None

    def __post_init__(self):
        """
        自動生成された__init__の後に処理を入れる
        """
        if (self.ar_from_ccx is not None) and (self.ar_from_ccy is not None):
            r = math.sqrt(self.ar_from_ccx ** 2 + self.ar_from_ccy ** 2)
            self.ar_r_from_cc = r
