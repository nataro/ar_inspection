import logging
from datetime import datetime

import click

from util import logger_tool

from processor.detect_ar import DetectArMarkers
from processor.detect_component import DetectConnectedComponents
from gui.ar_analyze_usb_cam import ArMarkerAnalyze

from util.datasets import InspectionResultDataset, get_keys_csv

## logging setting
logging.basicConfig(level=logging.ERROR, format='%(threadName)s: %(message)s')

# file_name_head
dt = datetime.now()
# logfile_head = './log_file/' + dt.strftime('%Y%m%d_%M:%S_')
logfile_head = dt.strftime('%Y%m%d_%H%M%S_')

# ar_analyze log
logger_name = 'gui.ar_analyze_usb_cam'
log_file = logfile_head + 'inspection_result.csv'
fm = '%(message)s'
header_csv = get_keys_csv(InspectionResultDataset())  # ロギングヘッダー用文字列を生成

logger_tool.get_logger_with_presets(
    logger_name=logger_name,
    log_file=log_file,
    header=header_csv,
    format=fm,
    # date_format=date_fm,
    # level=logging.DEBUG,
    level=logging.INFO,
)


@click.command()
@click.option('--marker_length_m', '-ml', default=0.012, type=float)
@click.option('--cam_file', '-cf', default='mtx.csv', type=str)
@click.option('--distortion_file', '-df', default='dist.csv', type=str)
@click.option('--cam_num', '-n', default=0, type=int)
@click.option('--cap_size', '-c', default=(1920, 1080), nargs=2, type=click.Tuple([int, int]))
@click.option('--disp_size', '-d', default=(1280, 720), nargs=2, type=click.Tuple([int, int]))
def cmd(
        marker_length_m,
        cam_file,
        distortion_file,
        cam_num,
        cap_size,
        disp_size,
):
    """
    Parameters
    ----------
    marker_length_m : 検出するマーカの寸法をmで指定
    cam_file : 読み込むカメラパラメータのファイル名
    distortion_file : 読み込む歪パラメータのファイル名
    cam_num : アクセスするUSBカメラデバイスのインデックス番号（自動で認識できなかった時に使用）
    cap_size : USBカメラデバイでキャプチャする画像サイズ
    disp_size : GUIで表示する際の画面サイズ
    """

    cam_mat, dist_mat = DetectArMarkers.load_and_check_parameter(cam_file, distortion_file)

    dar = DetectArMarkers(cam_mat, dist_mat, marker_length_m)
    dcc = DetectConnectedComponents()
    ara = ArMarkerAnalyze(
        detect_marker=dar,
        detect_componet=dcc,
        cap_size=cap_size,
        disp_size=disp_size,
        cam_num=cam_num,
    )
    ara.run()


def main():
    cmd()


if __name__ == '__main__':
    '''
    スペース入りの文字列を渡して実行する方法
    python ar_analyze.py -d 640 360 -ml 0.012 -n 0

    '''
    main()
