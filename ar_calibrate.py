import logging

import click

from processor.detect_ar import DetectChessBoard
from gui.ar_caribrate_usb_cam import ChessBoardCalibrate

## logging setting
logging.basicConfig(level=logging.ERROR, format='%(threadName)s: %(message)s')


@click.command()
@click.option('--square_length_m', '-sl', default=0.022, type=float)
@click.option('--col_intersections', '-ci', default=7, type=int)
@click.option('--row_intersections', '-ri', default=6, type=int)
@click.option('--cam_file', '-cf', default='mtx.csv', type=str)
@click.option('--distortion_file', '-df', default='dist.csv', type=str)
@click.option('--cam_num', '-n', default=0, type=int)
@click.option('--cap_size', '-c', default=(1920, 1080), nargs=2, type=click.Tuple([int, int]))
@click.option('--disp_size', '-d', default=(1280, 720), nargs=2, type=click.Tuple([int, int]))
def cmd(
        square_length_m,
        col_intersections,
        row_intersections,
        cam_file,
        distortion_file,
        cam_num,
        cap_size,
        disp_size,
):
    """
    Parameters
    ----------
    square_length_m : チェスボードの１桝の正方形寸法をmで指定
    col_intersections : チェスボードの列方向の交点数
    row_intersections : チェスボードの 行方向の交点数
    cam_file : 出力するカメラパラメータのファイル名
    distortion_file : 出力する歪パラメータのファイル名
    cam_num : アクセスするUSBカメラデバイスのインデックス番号（自動で認識できなかった時に使用）
    cap_size : USBカメラデバイでキャプチャする画像サイズ
    disp_size : GUIで表示する際の画面サイズ
    """
    dcb = DetectChessBoard(col_intersections, row_intersections, square_length_m)

    cbc = ChessBoardCalibrate(
        detect_chess=dcb,
        output_cam_file=cam_file,
        output_dist_file=distortion_file,
        cap_size=cap_size,
        disp_size=disp_size,
        cam_num=cam_num
    )
    cbc.run()


def main():
    cmd()


if __name__ == '__main__':
    '''
    スペース入りの文字列を渡して実行する方法
    python ar_analyze.py -d 640 360 -ml 0.012 -n 0

    '''
    main()
